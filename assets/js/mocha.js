$(document).ready( function() {

    $('.bt-menu').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('open');
        $('body').toggleClass('open');
        $('.navbar-fixed-top').toggleClass('open');
    });


    var a = $('main .content .container').offset().top;

    $('header').affix({
        offset: {
            top: a - 150
        }
    });


    $('.btn.btn-login').on('click', function() {
       $('.backdrop').fadeIn(300);
        $('.modal-account').delay(350).fadeIn(300);
    });
    $('.backdrop, .close-modal-account').on('click', function(){
        $('.backdrop').delay(350).fadeOut(300);
        $('.modal-account').fadeOut(300);
    });


});
