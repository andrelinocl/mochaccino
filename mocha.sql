-- --------------------------------------------------------
-- Servidor:                     localhost
-- Versão do servidor:           10.2.3-MariaDB-log - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para mocha
CREATE DATABASE IF NOT EXISTS `mocha` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mocha`;

-- Copiando estrutura para tabela mocha.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `content` text DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `author` tinyint(4) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mocha.posts: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `title`, `slug`, `image`, `content`, `status`, `author`, `date`) VALUES
	(1, 'ED DO EIUSMOD TEMPOR INCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA.', 'ed-do-eiusmod-tempor-incididunt-ut-labore-et-dolore-magna-aliqua', 'uploads/', '<p>\r\n    Pellentesque eget vehicula tellus. Sed interdum rhoncus tellus non condimentum. Aliquam quis eros vulputate, dictum metus sit amet, pretium velit. Nam purus nulla, imperdiet quis purus vel, aliquet consequat est. Donec gravida in diam sit amet fermentum. Suspendisse tincidunt arcu velit, vitae efficitur mi lobortis id. Phasellus nec dignissim nibh. Aliquam vehicula diam at ante sodales, vitae iaculis tortor tristique. Cras fringilla metus et eros viverra, vel varius lectus fringilla.</p>\r\n<p>\r\n    Curabitur iaculis diam faucibus lacus dapibus scelerisque. Vestibulum placerat fermentum erat, ut lobortis neque condimentum ac. Donec sit amet lacus est. Duis facilisis dapibus fermentum. Praesent id mattis lacus, et ultricies nulla. Donec arcu ipsum, egestas sed leo sed, commodo varius nulla. Integer et porta mi, at pulvinar dui. Donec suscipit metus ut mollis tincidunt. Ut scelerisque auctor libero. In et efficitur diam. Mauris cursus scelerisque pulvinar.\r\n</p>\r\n<p>\r\n    Sed sollicitudin vel metus vel laoreet. Pellentesque nec congue diam. Duis mollis faucibus augue, eleifend volutpat neque tempor ac. Sed nisl nisi, pretium sit amet enim sed, porta malesuada augue. Cras posuere, arcu sed aliquam commodo, tortor lectus congue dolor, mattis auctor metus quam id purus. Praesent sit amet auctor lacus, ut viverra erat. Sed non dolor blandit, feugiat erat a, aliquam enim. Duis varius mi eleifend malesuada placerat. Sed vestibulum quis sapien et blandit. Sed lacinia sem id nibh tincidunt, id accumsan magna luctus.\r\n</p>', '1', 1, '2017-07-04 23:30:56'),
	(2, 'SALMON DISH FOOD MEAL', 'salmon-dish-food-meal', 'salmon-dish-food-meal-46239.jpg', '<p>Nulla facilisi. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras ullamcorper rutrum ligula, vel lacinia quam auctor in. Vestibulum ex augue, dictum eget dolor semper, vulputate posuere nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed sit amet lacinia orci. Aliquam at eros elit. Curabitur viverra ex massa, ut gravida mi ultricies vel. Integer eleifend erat in vehicula porta. Nam et tincidunt nunc. Donec massa leo, ultrices ut mauris eget, pellentesque pellentesque odio. Donec tincidunt justo nisl, a bibendum nulla finibus vitae. Etiam pretium pretium erat eget aliquet. Ut nulla mauris, aliquam eu rutrum nec, sagittis quis neque.\r\n</p>\r\n<p>Vestibulum a feugiat dui. Curabitur consequat dolor massa, a auctor lorem consequat vel. Nullam eget malesuada nisi. Nunc placerat consequat risus, non aliquet magna molestie quis. Vivamus lacinia pharetra nisi, a efficitur ex pretium vitae. Cras a elementum dolor. Vivamus ac laoreet nunc. Fusce mollis scelerisque enim, at ullamcorper risus blandit sit amet. Sed et quam facilisis, porta ante ac, scelerisque diam. Morbi ut justo vulputate, ultrices dui sed, accumsan elit. Fusce egestas mi at neque congue pharetra. Mauris eget diam facilisis nunc sagittis congue quis sit amet turpis. Donec nisi eros, ullamcorper at ornare et, bibendum vel est.\r\n</p>\r\n<p>Integer maximus accumsan sem at eleifend. Sed eu ex non sapien viverra vehicula in a sapien. Vivamus arcu dui, tempus non tristique non, tincidunt sed urna. Fusce volutpat nulla vel enim dictum facilisis vel vitae ligula. Praesent ut lacinia turpis, sed sodales leo. Duis a elit ullamcorper, pulvinar lorem et, eleifend purus. Duis lorem dolor, elementum id tincidunt in, porta ut ex. Aenean at massa arcu.\r\n</p>', '1', 1, '2017-07-04 23:30:56'),
	(3, 'FOOD CHICKEN MEAT OUTDOORS', 'food-chicken-meat-outdoors', 'food-chicken-meat-outdoors.jpg', '<p>Pellentesque dapibus, quam id facilisis aliquet, ante ipsum ultricies magna, sit amet convallis lectus lorem eget augue. Sed mollis auctor felis, eget faucibus urna posuere et. In hac habitasse platea dictumst. Vivamus est odio, viverra sit amet erat quis, mollis imperdiet metus. Sed eget ligula dui. In commodo velit ligula, id blandit augue suscipit at. Ut mattis nisl eget hendrerit venenatis. Aenean facilisis enim non neque viverra placerat. Nulla nec nunc elementum, hendrerit dui a, tempor nunc. Donec dictum vulputate nibh, eget consectetur neque. Nulla magna ex, finibus sed dignissim nec, auctor a tellus. Duis imperdiet viverra sem, id iaculis lacus aliquet sed. Mauris non mi semper, lacinia est maximus, tristique sapien. Fusce ac felis posuere leo blandit imperdiet eu ac turpis.\r\n</p>\r\n<p>Proin mattis diam est, sit amet dictum velit luctus quis. Nulla consectetur purus quis nibh lobortis placerat. Suspendisse mollis, augue ut vulputate pretium, ligula sem eleifend augue, sit amet euismod nisi nulla quis nunc. Maecenas sed mauris sed purus dapibus mollis. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent in ligula in dui iaculis accumsan. Vestibulum at sem eu tortor dignissim volutpat id nec erat. Integer in justo pharetra dui dapibus hendrerit. Nullam sed tincidunt velit, vel euismod orci. Integer nunc mi, luctus in metus vel, mollis semper lacus. Maecenas auctor accumsan augue a placerat.\r\n</p>', '1', 1, '2017-07-04 23:30:55'),
	(4, 'vegetables italian pizza restaurant', 'vegetables-italian-pizza-restaurant', 'vegetables-italian-pizza-restaurant.jpg', '&lt;p&gt;Proin imperdiet a sapien ac laoreet. Suspendisse eget risus viverra mauris fringilla consequat quis ac neque. Duis mattis porttitor tempus. Quisque eu tempus lacus. Nulla facilisi. Morbi eget neque in turpis condimentum pellentesque. Nulla commodo at libero ut luctus. Sed bibendum dolor at est auctor rhoncus. Cras at congue ligula.&lt;/p&gt;&lt;p&gt;Phasellus vel maximus ligula, ut eleifend dui. Quisque nec risus mauris. Morbi mattis elit ut tempor cursus. Ut congue diam eros, eget sollicitudin dui vestibulum eget. Suspendisse id orci eget sapien gravida posuere ac sollicitudin nulla. Morbi at est aliquet, rutrum ante non, accumsan ex. Phasellus in eleifend elit. Suspendisse semper pellentesque turpis id tincidunt. Aliquam nec leo ornare, ultricies ante eu, tempor augue. Integer massa ligula, venenatis sed maximus et, malesuada a est. Aenean iaculis sapien erat, at dapibus ex placerat eget. Duis vulputate sodales sem, et bibendum est facilisis vel. Morbi nisl lectus, commodo ut urna scelerisque, sollicitudin mollis metus. In justo dui, pellentesque non consectetur vel, lobortis eu turpis. Sed sed euismod quam. Phasellus et rhoncus nibh.&lt;/p&gt;', '1', 1, '2017-07-04 23:30:55'),
	(5, 'Lorem Ipsum Teste', 'lorem-ipsum-teste', 'uploads/asdasdasdasdasda.jpg', '<p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Donec quis augue sed turpis iaculis aliquet blandit at dui. Curabitur quis metus fringilla, suscipit mi vel, tristique enim. Nulla augue augue, ornare in tristique eget, placerat at eros. Proin eu purus lobortis, molestie arcu eu, imperdiet tortor. Curabitur eu egestas nunc. Sed est nulla, fermentum eget dui quis, tempor dictum augue. Donec ullamcorper porta sollicitudin. Etiam efficitur accumsan elit, ut venenatis ligula congue eget. Donec vitae ipsum in enim accumsan tincidunt eget sit amet justo. Cras vel turpis id dolor viverra laoreet ac ut nisl. Phasellus scelerisque sapien eu commodo tincidunt. Quisque consectetur suscipit eros, rutrum tristique sapien tempus non. Nullam a elementum ex.</p><p style="margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;">Pellentesque sed neque ut sapien ultricies sagittis. Duis sed ipsum pharetra, luctus ligula ut, consectetur augue. Praesent dictum varius lorem ac egestas. Nam pulvinar enim in mollis accumsan. Proin vel condimentum justo, ac placerat tortor. Cras congue risus mi, eget sagittis dolor suscipit quis. Suspendisse potenti. Aenean vel velit sapien. Vivamus libero risus, porttitor non semper at, lobortis eu mauris. Aliquam tempus felis vel tortor fringilla, id blandit eros tristique. Aliquam cursus vestibulum nisl sed fermentum. Morbi nec tincidunt justo. Maecenas elit ante, maximus at fringilla eu, venenatis eu odio.</p>', '1', 1, '2017-07-04 23:30:55'),
	(6, 'tessteeee', 'tessteeee', 'uploads/', 'teste', '1', 9, '2017-07-04 21:27:53'),
	(7, 'tessteeee 321', 'tessteeee-321', 'pexels-photo-132694.jpeg', 'testes 321', '1', 9, '2017-07-04 21:46:30'),
	(8, 'Lorem Pexels Teste', 'lorem-pexels-teste', 'pexels-photo-450035.jpeg', 'Teste', '1', 9, '2017-07-04 21:49:00'),
	(9, 'Mais um teste', 'mais-um-teste', 'pexels-photo-450035.jpeg', 'mais um teste', '1', 9, '2017-07-04 21:56:11'),
	(10, 'teste para o alerta', 'teste-para-o-alerta', 'pexels-photo.jpg', 'asdasdasdad', '1', 9, '2017-07-04 21:57:28'),
	(11, 'teste', 'teste', 'pexels-photo-450035.jpeg', 'asdasdasda', '1', 9, '2017-07-04 21:58:31'),
	(12, 'teste 5648997', 'teste-5648997', 'pexels-photo-450035.jpeg', 'asdasdasdd', '1', 9, '2017-07-04 22:00:56');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Copiando estrutura para tabela mocha.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `permissions` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mocha.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `login`, `password`, `status`, `permissions`) VALUES
	(1, 'Rootesa', 'root', '1234', 1, 1),
	(9, 'André Carello', 'andre', '123654', 1, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
