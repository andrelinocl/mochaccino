<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tags extends MY_Controller {

	/* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'title',
            'label' => 'Titulo',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        )
    );

	/* CLASSE CONSTRUTORA */
	public function __construct(){
		parent::__construct();

        $this->load->model('tag_model');
		$this->load->library('form_validation');
		$this->load->helper('breadcrumb');
		$this->load->helper('message');
	}


    /* FUNCOES DE CARREGAMENTO DAS VIEWS */
	public function index(){
		$this->data += array(
			'get_all' => $this->tag_model->getAll(),
		);
		$this->load->view('template', $this->data);
	}

	public function create(){

        $this->form_validation->set_rules($this->validate);
        if($this->form_validation->run() == TRUE){

            $data = array(
                'title'		=> $this->input->post('title'),
                'author'	=> $this->session->id,
            );

            $result = $this->tag_model->create($data);
            if($result){
                $this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
            }else{
                $this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
            }
            redirect($this->router->fetch_class());
        }


		$this->data += array(

		);
		$this->load->view('template', $this->data);
	}

    public function update($id = NULL){

        $this->form_validation->set_rules($this->validate);
        if($this->form_validation->run() == TRUE){

            $data = array(
                'title'		=> $this->input->post('title'),
                'author'	=> $this->session->id,
            );

            $result = $this->tag_model->create($data);
            if($result){
                $this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
            }else{
                $this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
            }
            redirect($this->router->fetch_class());
        }


        $this->data += array(

        );
        $this->load->view('template', $this->data);
    }

    public function delete($id = NULL){
        if(!$id || !$this->tag_model->delete($id)):
            $this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
        else:
            $this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));
        endif;
        //redirect('blog');
    }

    /*
	public function delete($slug){
		if(!$slug || !$this->noticias_model->excluir($slug))
			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
		else
			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));

		redirect('blog');
	}
*/
}
