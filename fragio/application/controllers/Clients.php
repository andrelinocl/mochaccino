<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller {

	private $title = 'FRAGIO CMS';

	/* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'name',
            'label' => 'Nome',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        ),
		array(
			'field' => 'password',
			'label' => 'Senha',
			'rules' => 'trim|required|min_length[4]|max_length[8]',
			'errors' => array(
				'required' => 'Este campo é obrigatório',
				'min_length' => 'A %s precisa ter no mínimo 4 digitos',
				'max_length' => 'A %s precisa ter no máximo 8 digitos'
			),
		)
    );
	private $validate_login = array(
		'field' => 'login',
		'label' => 'Login',
		'rules' => 'trim|required|is_unique[users.login]',
		'errors' => array(
			'required' => 'Este campo é obrigatório',
			'is_unique' => 'O %s já está em uso'
		),
	);

	/* CLASSE CONSTRUTORA */
	public function __construct(){
		parent::__construct();

		$this->load->model('users_model');
		$this->load->library('form_validation');
		$this->load->helper('breadcrumb');
		$this->load->helper('message');
		$this->load->library('encryption');
	}


    /* FUNCOES DE CARREGAMENTO DAS VIEWS */
	public function index(){
		$this->data += array(
			'title' => $this->title,
			'get_all' => $this->users_model->getAll(),
		);
		$this->load->view('template', $this->data);
	}

	public function create(){

		$this->form_validation->set_rules($this->validate);
		$this->form_validation->set_rules($this->validate_login);
		if($this->form_validation->run() == TRUE):

			$data = array(
				'name' => $this->input->post('name'),
				'login' => $this->input->post('login'),
				'password' => $this->input->post('password'),
				'status' => $this->input->post('status')=='rd_active' ? 1 : 0,
			);

			$result = $this->users_model->create($data);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect($this->router->fetch_class().'/create');
		else:
			$this->data += array(
				'title' => $this->title,
			);
			$this->load->view('template', $this->data);
		endif;

	}

	public function edit($id = NULL){

		$this->form_validation->set_rules($this->validate);

		if($this->input->post('login') == $this->users_model->getLogin($this->input->post('login'))):
			$this->form_validation->set_rules($this->validate_login);
		endif;
		if($this->form_validation->run() == TRUE):

			$data = array(
				'name' => $this->input->post('name'),
				'login' => $this->input->post('login'),
				'password' => $this->input->post('password'),
				'status' => $this->input->post('status')=='rd_active' ? 1 : 0,
			);

			$result = $this->users_model->update($data, $id);
			if($result){
				$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
			}else{
				$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
			}
			redirect('blog');
		else:

			$this->data += array(
				'title' => $this->title,
			);
			$this->load->view('template', $this->data);
		endif;
	}

	public function delete($id){
		if(!$id || !$this->users_model->delete($id))
			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
		else
			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));

		redirect('clients');
	}



	/* FUNCOES PARA MANUPULACAO DO BANCO */
	public function store(){
	}



	public function update($id = NULL){


	}

//	public function delete($slug){
//		if(!$slug || !$this->noticias_model->excluir($slug))
//			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
//		else
//			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));
//
//		redirect('blog');
//	}

}
