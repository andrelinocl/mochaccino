<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller{

    private $validate = array(
        array(
            'field' => 'login',
            'label' => 'Login',
            'rules' => 'trim|required|min_length[3]|max_length[20]|alpha_numeric',
            'errors' => array(
                'required' => 'Informe um %s válido.',
                'min_length' => 'Informe um %s válido.',
                'max_length' => 'Informe um %s válido.',
                'alpha_numeric' => 'Informe um %s válido.',
            ),
        ),
        array(
            'field' => 'password',
            'label' => 'Senha',
            'rules' => 'trim|required|min_length[3]',
            'errors'=> array(
                'required' => 'Informe uma %s válida.',
                'min_length' => 'Informe uma %s válida.',
            ),
        ),
        //$this->form_validation->set_rules('field_name', 'Field Label', 'rule1|rule2|rule3',
        //array('rule2' => 'Error Message on rule2 for this field_name'));
    );

    public function __construct(){
        parent::__construct();
        $this->load->model('users_model');
        $this->load->library('encryption');
    }

    public function index(){
        $this->login();
    }


    public function login(){

        $this->load->library('form_validation');

        $data = array(
            'title' => 'FRAGIOCMS',
            'controller' => 'login',
            'action' => 'index',
        );

        $this->form_validation->set_rules($this->validate);

        if($this->form_validation->run()){

            $login = $this->input->post('login');
            $pass = $this->input->post('password');

            $verify_access = $this->users_model->access($login, $pass);

            if($verify_access === TRUE AND $this->users_model->getUser($login)[0]->permissions == 1){
                $this->session->set_userdata(
                    array(
                        'logged_in' => TRUE,
                        'username'  => $this->users_model->getUser($login)[0]->name,
                        'login'     => $this->users_model->getUser($login)[0]->login,
                        'password'  => $this->users_model->getUser($login)[0]->password,
                        'id'        => $this->users_model->getUser($login)[0]->id
                    )
                );
                redirect(base_url('home'));
            }
        }

        $this->load->view('template', $data);

    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('users/login');
    }
}
