<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

	private $title = 'FRAGIO CMS';

	/* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'title',
            'label' => 'Titulo',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        ),
        array(
            'field' => 'content',
            'label' => 'Texto',
            'rules' => 'trim|required',
			'errors' => array(
                'required' => 'Este campo é obrigatório',
            ),
        )
    );

	/* CLASSE CONSTRUTORA */
	public function __construct(){
		parent::__construct();

		$this->load->model(array('blog_model', 'tag_model'));
		$this->load->library('form_validation');
		$this->load->helper('breadcrumb');
		$this->load->helper('message');
	}


    /* FUNCOES DE CARREGAMENTO DAS VIEWS */
	public function index(){
		$this->data += array(
			'title' => $this->title,
			'get_all' => $this->blog_model->getAll()
		);
		$this->load->view('template', $this->data);
	}

	public function create(){

        $this->form_validation->set_rules($this->validate);
        if($this->form_validation->run() == TRUE){

            $this->upload_image('image');
            $data = array(
                'title'		=> $this->input->post('title'),
                'content'	=> $this->input->post('content'),
                'slug'		=> url_title(strtolower($this->input->post('title'))),
                'image'		=> $this->upload->data('file_name'),
                'status'	=> 1,
                'author'	=> $this->session->id,
                'tags_id'   => $this->input->post('tags_id')
            );

            $result = $this->blog_model->create($data);
            if($result){
                $this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
            }else{
                $this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
            }
            redirect($this->router->fetch_class());
        }


		$this->data += array(
			'title' => $this->title,
            'get_tags' => $this->blog_model->getTags()
		);
		$this->load->view('template', $this->data);
	}

	public function edit($id = NULL){
        $this->form_validation->set_rules($this->validate);
        if($this->form_validation->run() == TRUE){

            $this->upload_image('image');
            $data = array(
                'title'		=> $this->input->post('title'),
                'content'	=> $this->input->post('content'),
                'slug'		=> url_title(strtolower($this->input->post('title'))),
                'image'		=> $this->upload->data('file_name'),
                'status'	=> 1,
                'author'	=> $this->session->id,
                'tags_id'   => $this->input->post('tags_id')
            );

            //die(print_r($data));

            $result = $this->blog_model->update($data, $id);
            if($result){
                $this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
            }else{
                $this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser salvo. Tente novamente!'));
            }
            redirect($this->router->fetch_class());
        }

		$this->data += array(
			'title' => $this->title,
            'get_tags' => $this->blog_model->getTags()
		);
		$this->load->view('template', $this->data);
	}

	public function change_status($id = NULL){
	    $data['status'] = $this->input->post('status');
	    $this->blog_model->change_status($id, $data);
    }

	public function excluir($id){
		$this->delete($id);
	}



	public function delete($id){
		if(!$id || !$this->blog_model->delete($id))
			$this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro não pode ser excluído. Tente novamente!'));
		else
			$this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro excluído com sucesso.'));

        redirect($this->router->fetch_class());
	}
}
