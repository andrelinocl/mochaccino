<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	private $title = 'CMS';

	public function index()
	{
		$this->data += array(
			'title' => $this->title,
		);
		$this->load->view('template', $this->data);
	}
}
