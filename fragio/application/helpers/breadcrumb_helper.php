<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('create_breadcrumb')){
    function create_breadcrumb(){
        $ci = &get_instance();
        $i=1;
        $uri = $ci->uri->segment($i);
        $link = '<ol class="breadcrumb text-uppercase">';

        while($uri != ''){
            $prep_link = '';

            for($j=1; $j<=$i;$j++){
                $prep_link .= $ci->uri->segment($j).'/';
            }
            if($i-1 == 0){
                $link.='<li><a href="'.site_url().'">';
                $link.='Home</a></li> ';
            }
            if($ci->uri->segment($i+1) == ''){
                $link.='<li class="active">';
                $link.=$ci->uri->segment($i).'</li> ';
            }else{
                $link.='<li><a href="'.site_url($prep_link).'">';
                $link.=$ci->uri->segment($i).'</a></li> ';
            }

            $i++;
            $uri = $ci->uri->segment($i);
        }
            $link .= '</ol>';
            return $link;
    }
}
