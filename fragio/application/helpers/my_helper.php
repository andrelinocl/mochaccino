<?php

if (!function_exists('upload_url')) {
    function upload_url ($path) {
        if ($path) {
            return config_item('upload_dir') . $path;
        }
        return 'path não encontrada;';
    }
}