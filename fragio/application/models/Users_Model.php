<?php

class Users_Model extends CI_Model{

    private $tableUsers = 'users';

    public function access($login = NULL, $password = NULL){
		if( !$login && !$password ):
			return false;
		else:
			$this->db->where(
                array(
                    'login'=>$login,
                    'password'=>$password
                )
            );
			$query = $this->db->get($this->tableUsers);
			return $query->num_rows() > 0 ? true : false;
		endif;
	}

	public function logged_in(){
		if( $this->session->userdata('logged_in') !== true ):
			redirect(site_url('users/login'));
        endif;
	}

	public function getById($id = NULL){
		$this->db->where('id', $id);
		$this->db->limit(1);
		return $this->db->get($this->tableUsers);
	}

	public function getLogin($login = NULL){
		$this->db->where('login', $login);
		$query = $this->db->get($this->tableUsers);
		return $query->result();
	}

	public function getUser($login = NULL){
		$this->db->where('login', $login);
		$query = $this->db->get($this->tableUsers);
		return $query->result();
	}

	public function getAll(){
		$find = $this->db->get($this->tableUsers);
		return $find->result();
	}

	public function create($data = NULL){
		$this->db->insert($this->tableUsers, $data);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}

	public function update($data = NULL, $id = NULL){
		$this->db->where('id', $id);
		$this->db->update($this->tableUsers, $data);
		return $this->db->affected_rows() > 0 ? TRUE : FALSE;
	}

	public function delete($id = NULL){
		$this->db->where('id', $id);
		return $this->db->delete($this->tableUsers);
	}
}
