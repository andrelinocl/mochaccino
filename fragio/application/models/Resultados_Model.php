<?php

    class Resultados_Model extends CI_Model{

        private $tabela = 'partidas';

        public function buscar_todas(){
            $this->db->join('tipos_de_partida', 'tipos_de_partida.id_tipo_de_partida = '.$this->tabela.'.tipo_de_partida_id');
            $this->db->join('adversarios', 'adversarios.id_adversario = '.$this->tabela.'.adversario_id');
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function buscar_pelo_id($id = NULL){
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function buscar_pela_slug($slug = NULL){
            $this->db->where('slug', $slug);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function adicionar($dados = NULL){
            $this->db->insert($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function editar($dados = NULL, $id = NULL){
            $this->db->where('id', $id);
            $this->db->update($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function excluir($slug = NULL){
            $this->db->where('slug', $slug);
            return $this->db->delete($this->tabela);
        }

    }
