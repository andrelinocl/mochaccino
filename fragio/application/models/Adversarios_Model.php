<?php

    class Adversarios_Model extends CI_Model{

        private $tabela = 'adversarios';

        public function buscar_todas(){
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function buscar_pelo_id($id = NULL){
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function buscar_pela_slug($slug = NULL){
            $this->db->where('slug', $slug);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function adicionar($dados = NULL){
            $this->db->insert($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function editar($dados = NULL, $id = NULL){
            $this->db->where('id', $id);
            $this->db->update($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function excluir($slug = NULL){
            $this->db->where('slug', $slug);
            return $this->db->delete($this->tabela);
        }

    }
