<?php

    class Blog_Model extends CI_Model{

        private $table = 'posts';

        public function getAll(){
            $find = $this->db->get($this->table);
            return $find->result();
        }

        public function getById($id = NULL){
            $this->db->where('id', $id);
            $this->db->limit(1);
            return $this->db->get($this->table);
        }

        public function getBySlug($slug = NULL){
            $this->db->where('slug', $slug);
            $this->db->limit(1);
            return $this->db->get($this->table);
        }

        public function create($data = NULL){
            $this->db->insert($this->table, $data);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function update($data = NULL, $id = NULL){
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function delete($id = NULL){
            $this->db->where('id', $id);
            return $this->db->delete($this->table);
        }

        public function change_status($id = NULL, $data = NULL){
            $this->db->where('id', $id);
            $this->db->update($this->table, $data);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function getTags(){
            $this->db->from('tags');
            $this->db->order_by('id');
            $result = $this->db->get();
            $busca = array();
            if($result->num_rows() > 0) {
                foreach($result->result_array() as $row) {
                    $busca[$row['id']] = $row['title'];
                }
            }
            return $busca;
        }

        public function update_views($id = NULL, $views = NULL){
            $this->db->where('id', $id);
            $this->db->update($this->table, $views);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

    }
