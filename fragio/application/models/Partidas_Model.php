<?php

    class Partidas_model extends CI_Model{

        private $tabela = 'partidas';

        public function buscar_todas(){
            $this->db->join('tipos_de_partida', 'tipos_de_partida.id_tipo_de_partida = '.$this->tabela.'.tipo_de_partida_id');
            $this->db->join('adversarios', 'adversarios.id_adversario = '.$this->tabela.'.adversario_id');
            $busca = $this->db->get($this->tabela);
            return $busca->result();
        }

        public function busca_adversario(){
            $this->db->from('adversarios');
            $this->db->order_by('id_adversario');
            $result = $this->db->get();
            $busca = array();
            if($result->num_rows() > 0) {
                foreach($result->result_array() as $row) {
                    $busca[$row['id_adversario']] = $row['nome_adversario'];
                }
            }
            return $busca;
        }

        public function busca_tipos_de_partidas(){
            $this->db->from('tipos_de_partida');
            $this->db->order_by('id_tipo_de_partida');
            $result = $this->db->get();
            $busca = array();
            if($result->num_rows() > 0){
                foreach ($result->result_array() as $row) {
                    $busca[$row['id_tipo_de_partida']] = $row['tipo_de_partida'];
                }
            }
            return $busca;
        }

        public function buscar_pelo_id($id = NULL){
            $this->db->where('id_partida', $id);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function buscar_pela_slug($slug = NULL){
            $this->db->where('slug', $slug);
            $this->db->limit(1);
            return $this->db->get($this->tabela);
        }

        public function adicionar($dados = NULL){
            $this->db->insert($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function editar($dados = NULL, $id = NULL){
            $this->db->where('id_partida', $id);
            $this->db->update($this->tabela, $dados);
            if($this->db->affected_rows() > 0){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        public function excluir($id = NULL){
            $this->db->where('id_partida', $id);
            return $this->db->delete($this->tabela);
        }

    }
