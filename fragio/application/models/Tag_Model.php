<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tag_Model extends CI_Model{

    private $table = 'tags';

    public function getAll(){
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function getById($id = NULL){
        $this->db->where('id', $id);
        $this->db->limit(1);
        return $this->db->get($this->table);
    }

    public function create($data = NULL){
        $this->db->insert($this->table, $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function delete($id = NULL){
        $this->db->where('id', $id);
        return $this->db->delete($this->table);
    }
}
