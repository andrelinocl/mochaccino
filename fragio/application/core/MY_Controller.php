<?php defined('BASEPATH') OR exit('No direct script access allowed');

    Class MY_Controller extends CI_Controller{

        protected $data = array();

        public function __construct(){
            parent::__construct();
            $this->beforeLoader();
            $this->load->model('users_model');
            $this->load->helper('my_helper');
            $this->users_model->logged_in();
        }

        public function beforeLoader () {
            $this->data += array(
                'controller' => $this->router->fetch_class(),
    			'action' => $this->router->fetch_method()
            );
        }

        public function upload_image($image){
            $config['upload_path'] = '../uploads';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '2048';
            $config['max_width'] = '1920';
            $config['max_height'] = '1080';
            $config['remove_spaces'] = true;
            $config['overwrite'] = true;

            $this->load->library('upload', $config);

            //die(print_r($config));

            if ( ! $this->upload->do_upload($image)){
                $error = array('error' => $this->upload->display_errors());

                //die(print_r($error));
            }else{
                $data = array('upload_data' => $this->upload->data());
                //echo '<pre>';
                //die(print_r($data));
            }
        }
    }
