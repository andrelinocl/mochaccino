<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>TAGS</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('tags/create', 'ADICIONAR', array('class'=>'btn blue btn-block'));?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="tabela">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titulo</th>
                            <th>Data Publicação</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($get_all as $row): ?>
                        <tr>
                            <td><?php echo $row->id;?></td>
                            <td>
                                <a href="<?php echo base_url('tags/edit/' .$row->id );?>" class=""><?php echo $row->title;?></a>
                            </td>
                            <td><?php echo date('d/m/Y', strtotime($row->date));?></td>
                            <td>
                                <?php echo anchor('tags/editar/'.$row->id, '<i class="fa fa-pencil"></i>', array('class'=>'btn orange btn-sm'));?>
                                <?php anchor('javascript:void(0)', '<i class="fa fa-trash"></i>', array('class'=>'btn red btn-sm', 'data-action'=>'delete', 'data-id'=>$row->id));?>
                                <a href="javascript:void(0)" class="btn red btn-sm" data-action="delete" data-id="<?php echo $row->id;?>"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
