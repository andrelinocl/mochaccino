<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 colsm-12 col-xs-12">
                <?php echo form_label('Titulo')."\r\n";?>
                <?php echo form_input(array(
                                            'name'  => 'title',
                                            'id'    => 'title',
                                            'class' => 'form-control'
                                        ),
                                        set_value(
                                                'title',
                                                isset($get) ? $get->title : ''
                                        )
                                    );?>
                <?php echo form_error('title', '<div class="alert alert-warning" role="alert">','</div>');?>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($get) ? $get->id : '');?>
                <?php echo form_button(array(
                    'class' => 'btn blue',
                    'content' => 'Salvar',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('tags', 'Voltar', array('class' => 'btn red'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
