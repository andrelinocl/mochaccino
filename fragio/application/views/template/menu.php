<aside class="menu">
    <nav>
        <ul class="nav metismenu" id="menu">
            <li>
                <a href="#" aria-expanded="true">
                    <span class="fa fa-newspaper-o fa-lg" aria-hidden="true"></span>
                    <span class="">Posts</span>
                    <span class="fa arrow fa-fw"></span>
                </a>
                <ul aria-expanded="true" class="nav">
                    <li>
                        <a href="<?php echo base_url('blog');?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> Listar
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('blog/create');?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> Criar
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" aria-expanded="true">
                    <span class="fa fa-tags fa-lg" aria-hidden="true"></span>
                    <span>Tags</span>
                    <span class="fa arrow fa-fw"></span>
                </a>
                <ul aria-expanded="true" class="nav">
                    <li>
                        <a href="<?php echo base_url('tags');?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> Listar
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('tags/create');?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> Criar
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="<?php echo base_url('clients');?>" aria-expanded="true">
                    <span class="fa fa-users fa-lg" aria-hidden="true"></span>
                    <span class="">Usuários</span>
                    <span class="fa arrow fa-fw"></span>
                </a>
                <ul aria-expanded="true" class="nav">
                    <li>
                        <a href="<?php echo base_url('clients');?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> Listar
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('clients/create');?>">
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i> Criar
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
</aside>
