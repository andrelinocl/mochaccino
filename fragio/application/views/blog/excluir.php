<?php
    $id = $this->uri->segment(3);
    $busca = $this->noticias_model->buscar_pelo_id($id)->row();

    if($id == NULL):
        redirect('blog');
    endif;

    //print_r($busca);
    //die();
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h1>Notícias cadastradas</h1>
            <?php echo anchor('blog/adicionar', 'Nova');?>
            <?php echo anchor('blog', 'Voltar'); ?>
            <hr>
            <?php
                echo form_open_multipart('blog/excluir', array('class'=>'form'));
                echo form_label('Titulo')."\r\n";
                echo form_input(array('name'=>'titulo', 'class'=>'form-control', 'disabled'=>'disabled'),set_value('titulo',$busca->titulo));
                echo form_label('Slug')."\r\n";
                echo form_input(array('name'=>'slug', 'class'=>'form-control', 'disabled'=>'disabled'), set_value('slug', $busca->slug));
                echo form_label('Data de publicação')."\r\n";
                echo form_input(array('name'=>'data_publicacao', 'class'=>'form-control', 'disabled'=>'disabled'), set_value('data_publicacao', $busca->data_publicacao));
                echo form_label('Texto')."\r\n";
                echo form_textarea(array('name'=>'texto', 'class'=>'form-control', 'disabled'=>'disabled'), set_value('texto', $busca->texto));
                echo form_upload();
                echo form_hidden('id', $busca->id);
                echo form_submit('enviar', 'Enviar');
                echo form_close();
            ?>
        </div>
    </div>
</div>
