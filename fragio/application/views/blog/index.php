<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Notícias cadastradas</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('blog/adicionar', 'Novo Post', array('class'=>'btn blue btn-block'));?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="tabela">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Titulo</th>
                            <th>Status</th>
                            <th>Data Publicação</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($get_all as $row): ?>
                        <tr>
                            <td><?php echo $row->id;?></td>
                            <td>
                                <a href="<?php echo base_url('blog/edit/' .$row->id );?>" class=""><?php echo $row->title;?></a>
                            </td>
                            <td>
                                <input type="checkbox" name="status" id="<?php echo $row->id;?>" <?php echo $row->status == 1 ? 'checked' : '';?>/>
                                <label for="<?php echo $row->id;?>">
                                    <span class="ui"></span>
                                </label>
                            </td>
                            <td><?php echo date('d/m/Y', strtotime($row->date));?></td>
                            <td>
                                <?php echo anchor('blog/edit/'.$row->id, '<i class="fa fa-pencil"></i>', array('class'=>'btn orange btn-sm'));?>
                                <?php echo anchor('blog/excluir/'.$row->id, '<i class="fa fa-trash"></i>', array('class'=>'btn red btn-sm', 'onclick' => 'deletar(event, this)'));?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

