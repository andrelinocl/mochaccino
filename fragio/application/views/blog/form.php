<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 colsm-12 col-xs-12">
                <?php echo form_label('Titulo')."\r\n";?>
                <?php echo form_input(array(
                                            'name'  => 'title',
                                            'id'    => 'title',
                                            'class' => 'form-control'
                                        ),
                                        set_value(
                                                'title',
                                                isset($get) ? $get->title : ''
                                        )
                                    );?>
                <?php echo form_error('title', '<div class="alert alert-warning" role="alert">','</div>');?>
                <?php echo form_label('Slug')."\r\n";?>

                <?php echo form_input(array(
                                            'name'  => 'slug',
                                            'id'    => 'slug',
                                            'class' => 'form-control'
                                        ),
                                        set_value(
                                            'slug',
                                            isset($get) ? $get->slug : ''
                                        )
                                    );?>
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo form_label('Imagem')."\r\n";?>
                        <?php echo form_upload(array(
                                                    'name'=>'image',
                                                    'id'=>'imgInp',
                                                    'class'=>'form-control'
                                                ),
                                                set_value(
                                                        'image',
                                                        isset($get) ? $get->image : ''
                                                )
                                            );?>
                        <?php echo form_dropdown('tags_id', $get_tags, isset($get) ? $get->tags_id : '', array('class'=>'form-control'));?>
                    </div>

                    <div class="col-lg-6">
                        <label for="imgOut">Preview <small class="thin">(Imagem de destaque da notícia)</small></label>
                        <div class="img-thumb">
                            <img src="<?php echo isset($get) ? $this->config->item('upload_dir') .  $get->image : '' ;?>" alt="" id="imgOut"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <?php echo form_label('Texto')."\r\n";?>
                        <textarea name="content" class="summernote-textarea">
                            <?php echo isset($get) ? $get->content : set_value('content') ?>
                        </textarea>
                        <?php echo form_error('content', '<div class="alert alert-warning" role="alert">','</div>');?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($get) ? $get->id : '');?>
                <?php echo form_button(array(
                    'class' => 'btn blue',
                    'content' => 'Salvar',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('blog', 'Voltar', array('class' => 'btn red'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
