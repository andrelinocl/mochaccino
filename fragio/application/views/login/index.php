<?php if(!$this->session->userdata("usuario_logado")) : ?>
<div class="wrapper">
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <?php
                    echo form_open("users/login");
                    echo '<div class="form-group">';
                    echo form_input(array(
                            'name' => 'login',
                            'id' => 'login',
                            'class' => 'form-control',
                            'maxlenth' => '255',
                            'placeholder'=>'Login',
                            'autofocus'=>'autofocus',
                        ), set_value('login'));
                    echo form_error('login', '<div class="alert alert-danger" role="alert">','</div>');
                    echo '</div>';
                    echo '<div class="form-group">';
                    echo form_password(array(
                            'name' => 'password',
                            'id' => 'password',
                            'class' => 'form-control',
                            'maxlenth' => '255',
                            'placeholder'=>'Senha',
                        ));
                    echo form_error('password','<div class="alert alert-danger" role="alert">', '</div>');
                    echo '</div>';
                    echo form_button(array(
                        'class' => 'btn button blue pull-right',
                        'content' => 'Login',
                        'type' => 'submit',
                    ));
                    echo form_close();
                ?>
            </div>
        </div>
    </div>
    <div id="particles-js"></div>
</div>
<?php endif; ?>
