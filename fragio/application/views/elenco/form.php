<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <?php
                    /* NOME */
                    echo form_label('Nome Completo')."\r\n";
                    echo form_input(array('name'=>'nome', 'id'=>'nome', 'class'=>'form-control'),set_value('nome', isset($busca) ? $busca->nome : ''));
                    echo form_error('nome', '<div class="alert alert-warning" role="alert">','</div>');
                    /* NOME */
                    /* APELIDO */
                    echo form_label('Apelido <span class="thin">(Caso não seja preenchido na home aparecerá o nome do jogador)</span>')."\r\n";
                    echo form_input(array('name'=>'apelido', 'id'=>'apelido', 'class'=>'form-control'),set_value('apelido', isset($busca) ? $busca->apelido : ''));
                    echo form_error('apelido', '<div class="alert alert-warning" role="alert">','</div>');
                    /* APELIDO */
                ?>
            <?php
            /* CAMISA */
            echo form_label('Camisa')."\r\n";
            echo form_input(array('name'=>'camisa', 'id'=>'camisa', 'class'=>'form-control'),set_value('camisa', isset($busca) ? $busca->camisa : ''));
            echo form_error('camisa', '<div class="alert alert-warning" role="alert">','</div>');
            /* CAMISA */
            /* POSICAO */
            echo form_label('Posição');
            echo form_dropdown('posicao_id', $lista_posicao, isset($busca) ? $busca->posicao_id : '', array('class'=>'form-control'));
            /* POSICAO */
            ?>
            </div>
            <div class="col-lg-6">
                <label for="imgOut">Foto</label>
                <?php echo form_upload(array('name'=>'imagem', 'id'=>'imgInp', 'class'=>'form-control'), set_value('imagem', isset($busca) ? $busca->imagem : ''));?>
                <div class="img-thumb">
                    <img src="<?php echo isset($busca) ? '/bemamil/uploads/'.$busca->imagem : '' ;?>" alt="" id="imgOut"/>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo form_label('Texto')."\r\n";?>
                <?php echo form_textarea(array('name'=>'texto', 'id'=>'', 'class'=>'form-control summernote-textarea'), set_value('texto', isset($busca) ? $busca->texto : ''));?>
                <?php echo form_error('texto', '<div class="alert alert-warning" role="alert">','</div>');?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($busca) ? $busca->id : '');?>
                <?php echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('blog', '<span><i class="fa fa-arrow-left" aria-hidden="true"></i></span><span>Voltar</span>', array('class' => 'btn btn-default'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
