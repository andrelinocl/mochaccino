<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Elenco Atual</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('elenco/adicionar', 'novo jogador', array('class'=>'btn btn-primary btn-block'));?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="lista-noticias">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Jogador</th>
                            <th>Apelido</th>
                            <th>Camisa</th>
                            <th>Posição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($lista_elenco as $linha): ?>
                        <tr>
                            <td><?php echo $linha->id;?></td>
                            <td>
                                <?php echo $linha->nome;?>
                            </td>
                            <td><?php echo $linha->apelido;?></td>
                            <td><?php echo $linha->camisa;?></td>
                            <td>
                                <?php echo $linha->posicao;?>
                            </td>
                            <td>
                                <?php echo anchor('elenco/editar/'.$linha->slug, '<i class="fa fa-pencil"></i> Editar', array('class'=>'btn btn-warning btn-xs'));?>                        <?php echo anchor('elenco/excluir/'.$linha->id, '<i class="fa fa-trash"></i> Excluir', array('class'=>'btn btn-danger btn-xs'));?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
