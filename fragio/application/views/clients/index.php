<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Lista de Usuários</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('usuarios/adicionar', '+', array('class'=>'btn btn-primary btn-block'));?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
                <?php echo isset($_SESSION['item']) ? $_SESSION['item'] : ''; ?>
                <table class="table table-responsive" id="tabela">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nome</th>
                            <th>Status</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($get_all as $row): ?>
                        <tr>
                            <td><?php echo $row->id;?></td>
                            <td>
                                <a href="<?php echo base_url('clients/edit/' .$row->id);?>" class="link--simple"><?php echo $row->name;?></a>
                            </td>
                            <td>
                                <?php echo $row->status == 1 ? 'Ativo' : 'Inativo'; ?>
                            </td>
                            <td>
                                <?php echo anchor('clients/edit/'.$row->id, '<i class="fa fa-pencil"></i>', array('class'=>'btn orange btn-sm'));?>
                                <?php echo anchor('clients/delete/'.$row->id, '<i class="fa fa-trash"></i>', array('class'=>'btn red btn-sm'));?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
