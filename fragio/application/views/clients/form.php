<div class="panel panel-default panel-custom">
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 colsm-12 col-xs-12">
                <div class="form-group">
                    <?php
                    $data = array(
                        'name'          => 'status',
                        'id'            => 'status',
                        'value'         => 'rd_active',
                        'checked'       => TRUE,
                    );
                    ?>
                    <label><?php echo form_radio($data);?> Ativo</label>

                    <?php
                    $data = array(
                        'name'          => 'status',
                        'id'            => 'status',
                        'value'         => 'rd_inactive',
                        'checked'       => FALSE,
                    );
                    ?>
                    <label><?php echo form_radio($data);?> Inativo</label>
                </div>

                <div class="form-group">
                    <?php echo form_label('Nome')."\r\n";?>
                    <?php echo form_input(array('name'=>'name', 'id'=>'name', 'class'=>'form-control'),set_value('name', isset($get) ? $get->name: ''));?>
                    <?php echo form_error('name', '<div class="alert alert-warning" role="alert">','</div>');?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Login')."\r\n";?>
                    <?php echo form_input(array('name'=>'login', 'id'=>'login', 'class'=>'form-control'), set_value('login', isset($get) ? $get->login: ''));?>
                    <?php echo form_error('login', '<div class="alert alert-warning" role="alert">','</div>');?>
                </div>
                <div class="form-group">
                    <?php echo form_label('Senha')."\r\n";?>
                    <?php echo form_password(array('name'=>'password', 'id'=>'password', 'class'=>'form-control'),set_value('password', isset($get) ? $get->password: ''));?>
                    <?php echo form_error('password', '<div class="alert alert-warning" role="alert">','</div>');?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_hidden('id', isset($get) ? $get->id : '');?>
                <?php echo form_button(array(
                    'class' => 'btn btn-success',
                    'content' => '<span><i class="fa fa-floppy-o" aria-hidden="true"></i></span><span>Salvar</span>',
                    'type' => 'submit',
                ));?>
                <?php echo anchor('blog', '<span><i class="fa fa-arrow-left" aria-hidden="true"></i></span><span>Voltar</span>', array('class' => 'btn btn-default'));?>
            </div>
        </div>
    </div>
</div>
<?php echo form_close();?>
