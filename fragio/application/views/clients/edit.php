<?php
    $id = $this->uri->segment(3);
    $get = $this->users_model->getById($id)->row();

    if($id == NULL):
        redirect('clients');
    endif;
?>

<main>
    <div class="row">
        <div class="col-lg-12 bg--branco">
            <div class="page-header">
                <div class="row">
                    <div class="col-lg-10 col-md-8 col-sm-8 col-xs-12">
                        <h1>Edição de Notícias</h1>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-xs-12">
                        <?php echo anchor('clients', '<span></span><span>Voltar</span>', array('class'=>'btn btn-block btn-default')); ?>
                    </div>
                </div>
                <?php echo create_breadcrumb();?>
            </div>
            <div class="page-content">
            <?php
                echo form_open_multipart('clients/edit/'. $get->id, array('class'=>'form'));
                $this->load->view('clients/form', compact('get'));
            ?>
        </div>
    </div>
</main>
