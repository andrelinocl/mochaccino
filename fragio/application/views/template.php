<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <title><?php echo $title; ?></title>

    <?php echo link_tag('https://fonts.googleapis.com/css?family=Fira+Mono');?>
    <?php echo link_tag('https://fonts.googleapis.com/css?family=Fira+Sans:300,400,500,700,800'); ?>

    <?php echo link_tag('assets/font-awesome/css/font-awesome.min.css'); ?>
    <?php echo link_tag('assets/css/bootstrap.min.css'); ?>
    <?php echo link_tag('assets/lib/bootstrap-datepicker/css/bootstrap-datepicker3.css');?>
    <?php echo link_tag('assets/css/metis-menu.css');?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.css" integrity="sha256-zgaKkhKpXzSrPyXVfczHhygcPSHyhHD+PSWnq3LZHHs=" crossorigin="anonymous" />
    <?php echo link_tag('assets/css/datatable.css');?>
    <?php echo link_tag('assets/summernote-master/dist/summernote.css');?>
    <?php echo link_tag('assets/css/bootstrap-select.min.css');?>
    <?php echo link_tag('assets/css/chosen.css');?>
    <?php echo link_tag('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.css');?>
    <?php echo link_tag('assets/css/fragio.css'); ?>


    <script>
        var APP_ROOT = '<?php echo base_url();?>';
    </script>

</head>
<body<?php echo !$this->session->logged_in ? ' class="login"' : ''; ?>>

<?php if($this->session->logged_in):?>
 <nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
   <div class="navbar-header">
    <div class="user">
     <figure>
      <img class="img-responsive img-circle" src="<?php echo base_url('assets/imagem/no-user.jpg'); ?>" alt="" />
     </figure>
     <p>Bem Vindo<br><strong><?php echo $this->session->username; ?></strong></p>
    </div>
    <button type="button" class="btn">
     <i class="fa fa-bars"></i>
    </button>
   </div>
   <div class="navbar-links">
    <ul class="nav navbar-nav navbar-right">
     <li><a href="<?php echo base_url('users/logout');?>"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
    </ul>
   </div>
  </div>
 </nav>


    <?php  $this->load->view('template/menu'); ?>
<?php endif;?>


<?php
 $this->load->view($controller . '/' . $action);
 ?>


<?php echo script_tag('assets/js/jquery-2.2.4.min.js'); ?>
<?php echo script_tag('assets/js/bootstrap.min.js'); ?>
<?php echo script_tag('assets/js/particles.min.js');?>
<?php echo script_tag('assets/js/metis-menu.js');?>
<?php echo script_tag('assets/js/datatable.js');?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.6/sweetalert2.min.js" integrity="sha256-NBMm26+MVgnPpBR/jdmM0orRevP7j26HoHC3IPW/T+k=" crossorigin="anonymous"></script>
<?php echo script_tag('assets/js/datatable-bootstrap.js');?>
<?php echo script_tag('assets/js/jQuery.stringToSlug.js');?>
<?php echo script_tag('assets/lib/bootstrap-datepicker/js/bootstrap-datepicker.min.js');?>
<?php echo script_tag('assets/lib/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js');?>
<?php echo script_tag('assets/summernote-master/dist/summernote.min.js');?>
<?php echo script_tag('assets/summernote-master/lang/summernote-pt-BR.js');?>
<?php echo script_tag('assets/js/bootstrap-select.min.js');?>
<?php echo script_tag('https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.6.5/sweetalert2.min.js');?>
<?php echo script_tag('assets/js/main.js'); ?>
<?php echo script_tag('assets/js/functions.js'); ?>

<script type="text/javascript">
    function deletar (e, el) {
        e.preventDefault();
        var $this = $(el);
        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function () {
            window.location.href = $this.attr('href')
        })
    }
</script>
</body>
</html>
