/**
 * Created by Andre on 04/07/2017.
 */
$(document).ready( function () {

    var checkbox = $('input[type="checkbox"]'),
        s = $(".btn");

    // APP_ROOT esta setada no template.php

    $(checkbox).on('click', function () {

        var postStatus  = $(this).is(':checked') ? '1' : '0',
            postId      = $(this).attr('id');

        $.ajax({
            type: "POST",
            url: $(document).attr('location').href + '/change_status/'+postId,
            data: {
                id: postId,
                status: postStatus
            }
        });
    });
});