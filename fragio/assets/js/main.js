$(document).ready( function() {

    // $('.menu-content h4').on('click', function () {
    //
	 //    var ul = $(this).parent().find('ul.submenu');
    //     ul.toggleClass('open');
	 //    console.log(ul)
    // });
    //
    // $('.menu-content .close').on('click', function () {
    //
    //     $(this).closest('ul.submenu').removeClass('open');
    // });

	$('.selectpicker').selectpicker();

	$('.summernote-textarea').summernote({
		toolbar: [
			// [groupName, [list of button]]
			['style', ['style']],
			['font', ['bold', 'italic', 'underline', 'clear']],
			['fontsize', ['fontsize']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['height', ['height']],
			['table', ['table']],
			['insert', ['link', 'picture', 'hr']],
			['view', ['codeview']],
			['help', ['help']]
		],
		placeholder: 'Coloque o texto desejado aqui...',
		lang: 'pt-BR.min',
		height: 450,
		disableResizeEditor: true,
		htmlMode: true,
		lineNumbers: true,
		mode: 'text/html'
	});

	$('#tabela').DataTable({
		'lengthMenu': [ 5, 10 , 20, 30, 40],
		'language': {
            'lengthMenu': 'Mostre _MENU_ registros por página',
            'zeroRecords': 'Desculpe. Não encontramos nada cadastrado com a sua pesquisa',
            'info': 'Temos _PAGE_ de _PAGES_ páginas',
            'infoEmpty': 'Nenhum registro encontrado',
            'infoFiltered': '(Filtrado de _MAX_ registros)',
			'sSearch': 'Filtro de Pesquisa:',
			'oPaginate': {
		    	'sNext': '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
				'sPrevious': '<i class="fa fa-chevron-left" aria-hidden="true"></i>'
			}
        }
	});

	$("#title").stringToSlug({
        setEvents: 'keyup keydown blur',
		getPut: '#slug',
		space: '-',
		prefix: '',
		suffix: '',
		replace: '',
		AND: 'and',
		callback: false
    });

    $('#date').datepicker({
        todayBtn: "linked",
        clearBtn: true,
        language: "pt-BR",
        orientation: "bottom left",
        autoclose: true,
    });

	$("#imgInp").change(function(){
		readURL(this);
	});

	/* ========================================================================= */
	/* METIS MENU OPTIONS */
	$("#menu").metisMenu();


	/* ========================================================================= */
    /* HIDE SIDEBAR */
    $('.sidebar-hide-box').click(function(){
        $('aside').toggleClass('hide-sidebar');
        $('main, .navbar-default').toggleClass('full');
    });
    /* /.HIDE SIDEBAR */

	/* ========================================================================= */
	/* HIDE ALERT */
	$('.close-alert').click(function(){
		$(this).parent().slideUp(500);
	});
	/* /.HIDE ALERT */
});


function readURL(input) {
   if (input.files && input.files[0]) {
	   var reader = new FileReader();

	   reader.onload = function (e) {
		   $('#imgOut').attr('src', e.target.result);
	   }

	   reader.readAsDataURL(input.files[0]);
   }
}

/* -----------------------------------------------
/* How to use? : Check the GitHub README
/* ----------------------------------------------- */

/* To load a config file (particles.json) you need to host this demo (MAMP/WAMP/local)... */
/*
particlesJS.load('particles-js', 'particles.json', function() {
  console.log('particles.js loaded - callback');
});
*/

/* Otherwise just put the config content (json): */

particlesJS('particles-js',

{
  "particles": {
	"number": {
	  "value": 160,
	  "density": {
		"enable": true,
		"value_area": 800
	  }
	},
	"color": {
	  "value": "#ffffff"
	},
	"shape": {
	  "type": "circle",
	  "stroke": {
		"width": 0,
		"color": "#000000"
	  },
	  "polygon": {
		"nb_sides": 5
	  },
	  "image": {
		"src": "img/github.svg",
		"width": 100,
		"height": 100
	  }
	},
	"opacity": {
	  "value": 1,
	  "random": true,
	  "anim": {
		"enable": true,
		"speed": 1,
		"opacity_min": 0,
		"sync": false
	  }
	},
	"size": {
	  "value": 3,
	  "random": true,
	  "anim": {
		"enable": false,
		"speed": 4,
		"size_min": 0.3,
		"sync": false
	  }
	},
	"line_linked": {
	  "enable": false,
	  "distance": 150,
	  "color": "#ffffff",
	  "opacity": 0.4,
	  "width": 1
	},
	"move": {
	  "enable": true,
	  "speed": 1,
	  "direction": "none",
	  "random": true,
	  "straight": false,
	  "out_mode": "out",
	  "bounce": false,
	  "attract": {
		"enable": false,
		"rotateX": 600,
		"rotateY": 600
	  }
	}
  },
  "interactivity": {
	"detect_on": "canvas",
	"events": {
	  "onhover": {
		"enable": false,
		"mode": "repulse"
	  },
	  "onclick": {
		"enable": true,
		"mode": "repulse"
	  },
	  "resize": true
	},
	"modes": {
	  "grab": {
		"distance": 400,
		"line_linked": {
		  "opacity": 1
		}
	  },
	  "bubble": {
		"distance": 250,
		"size": 0,
		"duration": 2,
		"opacity": 0,
		"speed": 3
	  },
	  "repulse": {
		"distance": 100,
		"duration": 0.4
	  },
	  "push": {
		"particles_nb": 4
	  },
	  "remove": {
		"particles_nb": 2
	  }
	}
  },
  "retina_detect": true
}

);
