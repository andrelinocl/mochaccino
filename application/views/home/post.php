<?php
    $slug = $this->uri->segment(2);
    $get = $this->blog_model->getPostBySlug($slug)->row();

    if($slug == NULL):
        redirect(base_url());
    endif;
?>


<main>
    <section class="content onePost">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <article class="mainPost">
                        <div class="postInfo">
                            <h1><?php echo $get->title ?></h1>
                            <figure>
                                <img src="<?php echo base_url('uploads/' . $get->image); ?>" alt="<?php $get->title ?>" class="img-responsive" title="<?php echo $get->title ?>">
                            </figure>
                            <p>
                                Postado em: <strong><?php echo date('d \d\e F \d\e Y', strtotime($get->date));?></strong> | Por: <strong><a href="" class="link">André Carello</a></strong> |  <strong>0</strong> Comentários
                            </p>
                            <?php echo $get->content ?>
                        </div>
                    </article>
                    <div class="comments">
                        <h3><span>COMENTÁRIOS</span></h3>
                        <div class="comment">
                            <h5>José Augusto</h5>
                            <p><small>29 de Julho</small></p>
                            <p>
                                 vitae efficitur mi lobortis id. Phasellus nec dignissim nibh. Aliquam vehicula diam at ante sodales, vitae iaculis tortor tristique. Cras fringilla metus et eros viverra, vel varius lectus fringilla.
                            </p>
                        </div>
                        <div class="comment">
                            <h5>Roman Malito</h5>
                            <p><small>29 de Julho</small></p>
                            <p>
                                 Pellentesque nec congue diam. Duis mollis faucibus augue, eleifend volutpat neque tempor ac.
                            </p>
                        </div>

                        <form class="" action="index.html" method="post">
                            <div class="form-group">
                                <label for="comment">Deixe um comentário</label>
                                <textarea name="comment" class="form-control"></textarea>
                            </div>
                            <button type="button" name="button" class="btn btn-default">Enviar</button>
                        </form>
                    </div>
                </div>
                <aside class="col-xs-12 col-md-4">
                    <h3>POSTS RELACIONADOS</h3>
                    <ul>
                        <?php foreach($get_all as $row): ?>
                            <?php if($slug != $row->slug && $get->tags_id == $row->tags_id):?>
                                <li>
                                    <a href="<?php echo base_url('post/'.$row->slug); ?>"><?php echo $row->title; ?></a>
                                </li>
                            <?php endIf;?>
                        <?php endforeach; ?>
                    </ul>

                    <h3>ÚLTIMOS INSCRITOS</h3>
                    <ul>
                        <?php $count = 0; ?>
                        <?php foreach($get_users as $row):?>
                            <?php if($count <= 5 && $row->permissions != 1):?>
                                <li>
                                    <?php echo $row->name;?>
                                </li>
                            <?php endif; ?>
                            <?php $count++;?>
                        <?php endforeach; ?>
                    </ul>
                </aside>
            </div>
        </div>
    </section>
</main>