<?php

    // echo '<pre>';
    // die(print_r($get_all));
 ?>

    <main>
        <section class="content allPosts">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <?php $count = 0; ?>
                        <?php foreach ($get_all as $row): ?>
                            <?php if($count == 0): ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <article class="mainPost">
                                        <a href="<?php echo base_url('post/'.$row->slug); ?>">
                                            <figure>
                                                <img src="<?php echo base_url('/uploads/'.$row->image); ?>" alt="<?php echo $row->title; ?>" title="<?php echo $row->title; ?>" class="img-responsive">
                                            </figure>
                                        </a>
                                        <div class="postInfo">
                                            <h1><a href="<?php echo base_url('post/'.$row->slug); ?>"><?php echo $row->title; ?></a></h1>
                                            <?php echo character_limiter($row->content, 300); ?>
                                            <p>
                                                Postado em: <strong><?php echo date('d \d\e M \d\e Y', strtotime($row->date));?></strong> |
                                                <?php foreach($get_users as $author):?>
                                                    <?php if($row->author == $author->id): ?>
                                                        Por: <strong><a href="" class="link"><?php echo $author->name; ?></a></strong>
                                                    <?php endIf; ?>
                                                <?php endforeach;?>
                                                |  <strong>0</strong> Views
                                            </p>
                                            <a href="<?php echo base_url('post/'.$row->slug) ?>" class="btn btn-default">Leia o artigo</a>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <div class="col-xs-12">
                                    <article class="defaultPost">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-4">
                                                <a href="<?php echo base_url('post/'.$row->slug); ?>">
                                                    <figure>
                                                        <img src="<?php echo base_url('/uploads/'.$row->image); ?>" alt="<?php echo $row->title; ?>" title="<?php echo $row->title; ?>">
                                                    </figure>
                                                </a>
                                            </div>
                                            <div class="col-xs-12 col-sm-8">
                                                <div class="postInfo">
                                                    <h1><a href="<?php echo base_url('post/'.$row->slug); ?>"><?php echo $row->title; ?></a></h1>
                                                    <?php echo character_limiter($row->content, 300); ?>
                                                    <p>
                                                        Postado em: <strong><?php echo date('d \d\e M \d\e Y', strtotime($row->date));?></strong> |
                                                        <?php foreach($get_users as $author):?>
                                                            <?php if($row->author == $author->id): ?>
                                                                Por: <strong><a href="" class="link"><?php echo $author->name; ?></a></strong>
                                                            <?php endIf; ?>
                                                        <?php endforeach;?>
                                                        |  <strong>0</strong> Views
                                                    </p>
                                                    <a href="<?php echo base_url('post/'.$row->slug) ?>" class="btn btn-default">Leia o artigo</a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php $count++; ?>
                        <?php endforeach; ?>
                    </div>
                    <aside class="col-xs-12 col-md-4">
                        <?php $this->load->view('template/sidebar'); ?>
                    </aside>
                </div>
            </div>
        </section>
    </main>
