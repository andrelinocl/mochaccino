<footer>
    <div class="footerContent">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <h4>CONTA</h4>
                    <ul>
                        <li><a href="#" class="link pink">Login</a></li>
                        <li><a href="#" class="link pink">Criar um conta</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <h4>TAGS</h4>
                    <ul>
                        <li><a href="#" class="link pink">Sobremesas</a></li>
                        <li><a href="#" class="link pink">Peixes</a></li>
                        <li><a href="#" class="link pink">Massas</a></li>
                        <li><a href="#" class="link pink">Assados</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <h1>Mocha</h1>
                </div>
            </div>
        </div>
    </div>
</footer>


<div class="backdrop"></div>
<div class="modal-account">
    <div class="modal-content">
        <a href="javascript:void(0)" class="close-modal-account"><i class="fa fa-times"></i></a>
        <h3>CRIAR UMA CONTA</h3>
        <?php echo form_open('users/account', array('class'=>'form-login')); ?>
        <?php echo form_label('Nome Completo')."\r\n";?>
        <?php echo form_input(array('name'=>'name', 'id'=>'name', 'class'=>'form-control'),set_value('name'));?>
        <?php echo form_label('Usu�rio')."\r\n";?>
        <?php echo form_input(array('name'=>'login', 'id'=>'login', 'class'=>'form-control'),set_value('login'));?>
        <?php echo form_label('Senha')."\r\n";?>
        <?php echo form_password(array('name'=>'password', 'id'=>'password', 'class'=>'form-control'),set_value('password'));?>
        <?php echo form_button(array(
            'class' => 'btn btn-login',
            'content' => 'Entrar',
            'type' => 'submit',
        ));?>
        <?php echo form_close();?>
    </div>
</div>

<script type="text/javascript" src="<?php echo base_url('assets/js/lib/jquery-2.2.4.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/lib/bootstrap-3.3.7.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/mocha.js') . '?' . time(); ?>"></script>

</body>
</html>
