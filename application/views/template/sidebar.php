<?php

$url = $this->uri->segment(2);

?>

<h3>POSTS RELACIONADOS</h3>
<ul>
    <?php foreach($get_all as $row): ?>
    <?php if($url != $row->slug):?>
    <li>
        <a href="<?php echo base_url('post/'.$row->slug); ?>"><?php echo $row->title; ?></a>
    </li>
    <?php endIf;?>
    <?php endforeach; ?>
</ul>

<h3>ÚLTIMOS INSCRITOS</h3>
<ul>
    <?php $count = 0; ?>
    <?php foreach($get_users as $row):?>
    <?php if($count <= 5 && $row->permissions != 1):?>
    <li>
        <?php echo $row->name;?>
    </li>
    <?php endif; ?>
    <?php $count++;?>
    <?php endforeach; ?>
</ul>