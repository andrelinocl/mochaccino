<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_Model extends CI_Model{

    private $table = 'users';


    public function addUser($data = NULL){
        $this->db->insert($this->table, $data);
        if($this->db->affected_rows() > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    public function getAll(){
        $this->db->select('name, login, id, permissions');
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->table);
        return $query->result();
    }


}