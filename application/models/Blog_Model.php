<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_Model extends CI_Model{
    function __construct() {
        $this->tablePosts = 'posts';
    }


    public function getAllPosts(){
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get($this->tablePosts);
        return $query->result();
    }

    public function getPostBySlug($slug = NULL){
        $this->db->where('slug', $slug);
        return $this->db->get($this->tablePosts);

    }

}
