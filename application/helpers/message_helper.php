<?php defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('create_message')){
    function create_message($type = NULL, $icon = NULL, $msg = NULL){
        $alert  = "";
        $alert .= '<div class="alert alert-'. $type .' alert-dismissible" role="alert">';
        $alert .= '<a href="javascrip:void(0)" class="close close-alert">';
        $alert .= '</a>';
        $alert .= '<p><i class="fa fa-'. $icon .'" aria-hidden="true"></i> '. $msg .'</p>';
        $alert .= '</div>';
        return $alert;
    }
}
