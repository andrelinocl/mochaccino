<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {


    /* VALIDACAO DO FORMULARIO */
    private $validate = array(
        array(
            'field' => 'name',
            'label' => 'Nome',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => 'Este campo � obrigat�rio',
            ),
        ),
        array(
            'field' => 'login',
            'label' => 'Login',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => 'Este campo � obrigat�rio',
            ),
        ),
        array(
            'field' => 'password',
            'label' => 'Senha',
            'rules' => 'trim|required',
            'errors' => array(
                'required' => 'Este campo � obrigat�rio'
            )
        )
    );


    public function __construct(){
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('message');
        $this->load->model('users_model');
    }

	public function index()	{
		$this->account();
	}

    public function account(){

        $this->form_validation->set_rules($this->validate);

        if($this->form_validation->run() == TRUE):

            $data = array(
                'name' => $this->input->post('name'),
                'login' => $this->input->post('login'),
                'password' => md5($this->input->post('password')),
                'permissions' => 2,
                'status' => 1
            );
            $result = $this->users_model->addUser($data);

            if($result){
                $this->session->set_flashdata('item', create_message('success', 'thumbs-up', 'Registro Salvo com sucesso'));
            }else{
                $this->session->set_flashdata('item', create_message('danger', 'thumbs-down', 'O Registro n�o pode ser salvo. Tente novamente!'));
            }
            redirect(base_url());

        endIf;
	}
}
